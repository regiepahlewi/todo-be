'use strict'

const chai = require('chai');
const chaiHttp = require('chai-http');
const app = require('../app/app');

// Configure chai
chai.use(chaiHttp);
chai.should();

describe("To Do", () => {
    describe("GET /", () => {

        it("should get all to do", (done) => {
            chai.request(app)
                .get('/todo')
                .end((err, res) => {
                    res.should.have.status(200);
                    res.body.should.be.a('object');
                    done();
                });
        });

        it("should get to do by task id", (done) => {
            const taskId = 1;
            chai.request(app)
                .get(`/todo/${taskId}`)
                .end((err, res) => {
                    res.should.have.status(200);
                    res.body.should.be.a('object');
                    done();
                });
        });

        it("should not get task id", (done) => {
            const taskId = 99;
            chai.request(app)
                .get(`/todo/${taskId}`)
                .end((err, res) => {
                    res.should.have.status(400);
                    done();
                });
        });

        it("add task with correct data", (done) => {
            const task = {
                task: 'Task From Unit Test',
                priority: 1
            };
            chai.request(app)
                .post(`/todo/`).send(task)
                .end((err, res) => {
                    res.should.have.status(201);
                    done();
                });
        });

        it("add task with in correct data priority 0 because priority only 1 - 3", (done) => {
            const task = {
                task: 'Task From Unit Test',
                priority: 0
            };
            chai.request(app)
                .post(`/todo/`).send(task)
                .end((err, res) => {
                    res.should.have.status(400);
                    done();
                });
        });

        it("update task with in correct data", (done) => {
            const taskId = 1;
            const task = {
                task: 'Task From Unit Test Edited',
                priority: 1
            };
            chai.request(app)
                .put(`/todo/${taskId}`).send(task)
                .end((err, res) => {
                    res.should.have.status(200);
                    done();
                });
        });

        it("update task with in correct task id ", (done) => {
            const taskId = 99;
            const task = {
                task: 'Task From Unit Test Edited',
                priority: 1
            };
            chai.request(app)
                .put(`/todo/${taskId}`).send(task)
                .end((err, res) => {
                    res.should.have.status(400);
                    done();
                });
        });

        it("update task with correct task id but priority in correct", (done) => {
            const taskId = 1;
            const task = {
                task: 'Task From Unit Test Edited',
                priority: 5
            };
            chai.request(app)
                .put(`/todo/${taskId}`).send(task)
                .end((err, res) => {
                    res.should.have.status(400);
                    done();
                });
        });

        it("delete task with correct data", (done) => {
            const taskId = 4;
            chai.request(app)
                .delete(`/todo/${taskId}`)
                .end((err, res) => {
                    res.should.have.status(200);
                    done();
                });
        });

        it("delete task with in correct task id", (done) => {
            const taskId = 99;
            chai.request(app)
                .delete(`/todo/${taskId}`)
                .end((err, res) => {
                    res.should.have.status(400);
                    done();
                });
        });
    });
});