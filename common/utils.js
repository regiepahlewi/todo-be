'use strict'


var utils = {
    errorResponse(req, res, err, msg = '') {
        if (err.name === 'SequelizeValidationError') {

            const errors = [];
            for (let i = 0; i < err.errors.length; i++) {
                errors.push(err.errors[i].message);
            }

            res.status(400);
            res.json({ statusCode : 400, message: 'Bad Request', validation : errors });
        } else {
            res.status(500)
            res.json({ tatusCode : 500, message: 'Internal Server Error', error : err })
        }

    },

    noDataUpdatedResponse(res) {
        res.status(200)
        res.json({ data: {}, message: 'No data updated' })
    },

    noDataFound(res) {
        res.status(200)
        res.json({ data: {}, message: 'No data found' })
    },

    noDataFoundPagination(res) {
        res.json({
            countPage: 0,
            countData: 0,
            list: []
        })
    },

    dataAlreadyFound(res) {
        res.status(200)
        res.json({ data: {}, message: 'data found' })
    },

    noDataDeleted(res) {
        res.status(200)
        res.json({ data: {}, message: 'No data deleted' })
    },

    noDataInserted(res) {
        res.status(200)
        res.json({ data: {}, message: 'No data inserted' })
    },

    capitalizeFirstLetterm(str) {
        var splitStr = str.toLowerCase().split(' ');
        for (var i = 0; i < splitStr.length; i++) {
            // You do not need to check if i is larger than splitStr length, as your for does that for you
            // Assign it back to the array
            splitStr[i] = splitStr[i].charAt(0).toUpperCase() + splitStr[i].substring(1);
        }
        // Directly return the joined string
        return splitStr.join(' ');
    },

    eliminateDuplicate(data) {
        if (data.length > 0) {

            var newData = [];
            var custIdUnique = 0;
            var custIdUniqueArr = [];

            for (var i = 0; i < data.length; i++) {
                if (custIdUnique !== data[i].customer_id) {
                    custIdUnique = data[i].customer_id;
                    if (custIdUniqueArr.filter(a => a === custIdUnique).length === 0) {
                        custIdUniqueArr.push(custIdUnique);
                        newData.push({
                            customer_id: data[i].customer_id,
                            full_name: data[i].full_name,
                            photo_url: data[i].photo_url
                        })
                    }
                }
            }

            return newData;
        } else {
            return [];
        }
    },
}

module.exports = utils