## To Do API with NodeJs
this project using NodeJs & MariaDB
 
## Installation
To install all dependency, please run this command.

```bash
npm install
```
or
```bash
yarn install
```

## Migration DB
edit database config at ```.env``` file
```
DB_HOST = localhost
DB_NAME = todo
DB_USER = todo
DB_PORT = 3306
DB_PASSWORD = s1nd0r0sumb1n6
``` 
and please run this query

```
CREATE DATABASE `todo` /*!40100 DEFAULT CHARACTER SET utf8mb4 */;

-- todo.todo definition

CREATE TABLE `todo` (
  `task_id` int(11) NOT NULL AUTO_INCREMENT,
  `task` varchar(255) NOT NULL,
  `priority` int(11) NOT NULL,
  `created_date` timestamp NULL DEFAULT NULL,
  `updated_date` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`task_id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8mb4;

```

## Runing
To run this project, please run this command.
```bash
npm start
```
or
```bash
yarn start
```

## Unit Test
For this project, unit test using mocha & chai.
```bash
npm test
```
or
```bash
yarn test
```

## Dockerize
This project suppport dockerize, please enter ```docker-compose``` folder, and please run ```docker-compose up -d```, and run db migration at mariaDB from docker compose
```
CREATE TABLE `todo` (
  `task_id` int(11) NOT NULL AUTO_INCREMENT,
  `task` varchar(255) NOT NULL,
  `priority` int(11) NOT NULL,
  `created_date` timestamp NULL DEFAULT NULL,
  `updated_date` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`task_id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8mb4;

```

## Author
Regie Algi Pahlewi
