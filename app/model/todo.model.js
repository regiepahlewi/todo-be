'use strict'

const { Sequelize } = require('sequelize');
const db = require('../../config/database');

const todo = db.define('todo', {
    taskId: {
        type: Sequelize.INTEGER,
        primaryKey: true,
        autoIncrement: true,
        allowNull: false,
        field: 'task_id'
    },
    task: {
        type: Sequelize.STRING,
        allowNull: false,
        validate: {
            notNull: {
                msg: 'task is required'
            }
        }
    },
    priority: {
        type: Sequelize.INTEGER,
        allowNull: false,
        validate: {
            notNull: {
                msg: 'priority is required'
            },
            min: {
                args: 1,
                msg : 'priority just 1 to 3'
            },
            max: {
                args: 3,
                msg : 'priority just 1 to 3'
            }
        }
    },
    createdDate: {
        type: Sequelize.DATE,
        allowNull: true,
        defaultValue: null,
        field: 'created_date'
    },
    updatedDate: {
        type: Sequelize.DATE,
        allowNull: true,
        defaultValue: null,
        field: 'updated_date'
    },
}, {
    timestamps: true,
    createdAt: 'createdDate',
    updatedAt: 'updatedDate',
    paranoid: false,
    underscored: true,
    freezeTableName: true
});

module.exports = todo
