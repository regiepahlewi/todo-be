'use strict'

const utils = require('../../common/utils');
const todo = require('../model/todo.model');

// Insert To Do
async function insertTodo(req, res) {
    try {
        await todo.create({
            task: req.body.task,
            priority: req.body.priority
        });

        res.status(201);
        res.json({
            message: 'Success!'
        });

    } catch (ex) {
        utils.errorResponse(req, res, ex);
    }
}

// Update To Do
async function updateTodo(req, res) {
    try {
        const data = await todo.update({
            task: req.body.task,
            priority: req.body.priority
        }, {
            where: {
                taskId: req.params.taskId
            }
        });

        if (data > 0) {
            res.json({
                message: 'Success!'
            });
        } else {
            res.status(400);
            res.json({ message: `to do unavailable` });
        }

    } catch (ex) {
        utils.errorResponse(req, res, ex);
    }
}

// Get All To Do
async function getAllTodo(req, res) {
    try {
        const data = await todo.findAll();
        res.json({
            message: 'Get All Todo',
            data: data
        });
    } catch (ex) {
        utils.errorResponse(req, res, ex);
    }
}

// Get To Do By Id
async function getTodoById(req, res) {
    try {
        const data = await todo.findAll({ where: { taskId: req.params.taskId } });

        if (data.length > 0) {
            res.json({
                message: 'Get Todo By id',
                data: data[0]
            });
        } else {
            res.status(400);
            res.json({ message: `data with id ${req.params.taskId} is not in to do list` });
        }

    } catch (ex) {
        utils.errorResponse(req, res, ex);
    }
}

// Delete To Do
async function deleteTodo(req, res) {
    try {
        const data = await todo.destroy({ where: { taskId: req.params.taskId } });

        if (data > 0) {
            res.json({ message: 'to do deleted' });
        } else {
            res.status(400);
            res.json({ message: `to do unavailable` });
        }

    } catch (ex) {
        utils.errorResponse(req, res, ex);
    }
}

const controller = {
    insertTodo,
    updateTodo,
    getAllTodo,
    getTodoById,
    deleteTodo
}

module.exports = controller;