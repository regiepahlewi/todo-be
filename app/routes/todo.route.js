'use strict'

const express = require('express');
const router = express.Router();
const controller = require('../controllers/todo.controller');

router.post('/', controller.insertTodo);
router.put('/:taskId', controller.updateTodo);
router.get('/', controller.getAllTodo);
router.get('/:taskId', controller.getTodoById);
router.delete('/:taskId', controller.deleteTodo);

module.exports = router;