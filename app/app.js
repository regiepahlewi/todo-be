const express = require('express');
const logger = require('morgan');
const cookieParser = require('cookie-parser');
const bodyParser = require('body-parser');
const cors = require('cors');
const helmet = require('helmet');
const dotenv = require('dotenv').config({ path: __dirname + '/./../.env' });

const fs = require('fs')
const path = require('path')

// module
const todo = require('./routes/todo.route');

const app = express();

// view engine setup
app.set('views', path.join(__dirname, '../views'));
app.set('view engine', 'jade');

app.use(bodyParser.json({ limit: '50mb' }));
app.use(bodyParser.urlencoded({ extended: true, limit: '50mb', parameterLimit: 50000 }));
app.use(cookieParser());

// set env mode
app.set('env', process.env.EXPRESS_ENV);

// set middleware
app.use(cors());
app.use(helmet());
app.use(logger(process.env.LOGGER));

// set route module
app.use('/todo/', todo);

// catch 404 and forward to error handler
app.use(function (req, res, next) {
    var err = new Error('Not Found');
    err.status = 404;
    next(err);
});

// error handler
app.use(function (err, req, res, next) {
    // set locals, only providing error in development
    res.locals.message = err.message;
    res.locals.error = req.app.get('env') === 'development' ? err : {};

    // render the error page
    res.status(err.status || 500);
    res.render('error');
});

// set port
app.set('port', process.env.PORT);
app.listen(app.get('port'), () => {
    console.log('App module listening on port', app.get('port'));
    console.log('Datetime', new Date());
    console.log('Timezone', process.env.TZ);
});

module.exports = app;