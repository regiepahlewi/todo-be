FROM node:lts-alpine as build
LABEL maintainer="Regie Pahlewi <regiealgipahlewi@gmail.com>"
WORKDIR /app
COPY . /app/

RUN apk add -U tzdata
RUN cp /usr/share/zoneinfo/Asia/Jakarta /etc/localtime
RUN date

RUN npm install --production
RUN npm install pm2 -g

EXPOSE 3000
CMD ["pm2-runtime","pm2-process.yml"]